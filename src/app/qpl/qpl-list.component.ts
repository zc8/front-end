import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { QPLListService } from './shared/qpl-list.service';

@Component({
  templateUrl: 'qpl-list.component.html'
})
export class QPLListComponent implements OnInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'isQualified',
    'partNumber',
    'partName',
    'revision',
    'toolDieSetNumber',
    'openPo',
    'supplierName',
    'supplierCode',
    'jurisdiction',
    'classification',
    'ctq',
    'qplExpirationDate',
    'lastUpdatedBy',
    'lastUpdatedDate'
  ];

  parts: any[];
  dataSource = new MatTableDataSource(this.parts);

  constructor(private _qplListService: QPLListService) { }

  ngOnInit() {
    this._qplListService.getParts().subscribe(data => {
      this.parts = data;
      this.dataSource = new MatTableDataSource(this.parts);

      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;

    });

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}

