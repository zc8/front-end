import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IQPLPart } from '../qpl-list.model';
import { Observable } from 'rxjs';

@Injectable()
export class QPLListService {
  private _url: string = 'https://exam.net-inspect.com/qpl?offset=0&pageSize=20';

  constructor(private http: HttpClient) { }

  getParts(): Observable<IQPLPart[]> {
    return this.http.get<IQPLPart[]>(this._url);
  }
}
